# 3. highgui
Tutorials from Highgui module.

The repo includes sample codes teaching how to read/save image/video files and how to use the built-in graphical user interface of OpenCV 3.


    git clone https://github.com/cvtutor/highgui.git 03highgui


## OpenCV Doc

* [High Level GUI and Media (highgui module)](http://docs.opencv.org/3.1.0/d0/de2/tutorial_table_of_content_highgui.html)


### CVTutor.HighGUI.Trackbar01

* [Adding a Trackbar to our applications!](http://docs.opencv.org/3.1.0/da/d6a/tutorial_trackbar.html)



